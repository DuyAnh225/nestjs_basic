-- AlterTable
ALTER TABLE `user` ADD COLUMN `roles` INTEGER NOT NULL DEFAULT 1,
    MODIFY `image` VARCHAR(191) NOT NULL DEFAULT 'uploads/avatar/no_avatar.png';
