import {
  Body,
  Controller,
  Post,
  Get,
  Query,
  ParseIntPipe,
  Param,
  Put,
  Delete,
  UploadedFile,
  Req,
  UseInterceptors,
  BadRequestException,
} from '@nestjs/common';
import {
  CreateUserDto,
  UpdateUserDto,
  UserFilterType,
  UserPaginationResponseType,
} from './dto/user.dto';
import { UserService } from './user.service';
import { User } from '@prisma/client';
import { Roles } from 'src/auth/decorator/role.decorator';
import {
  ApiBearerAuth,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { storageConfig } from 'helpers/config';
import { extname } from 'path';
@ApiBearerAuth()
@ApiTags('Users')
@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Post()
  @Roles('admin')
  create(@Body() body: CreateUserDto): Promise<User> {
    console.log('create user api ', body);
    return this.userService.create(body);
  }

  @ApiQuery({ name: 'page' })
  @ApiQuery({ name: 'items_per_page' })
  @ApiQuery({ name: 'search' })
  @Get()
  @Roles('admin')
  @ApiResponse({ status: 200, description: 'get list post successful' })
  @ApiResponse({ status: 200, description: 'get list post error' })
  getAll(@Query() params: UserFilterType): Promise<UserPaginationResponseType> {
    console.log('get all user api ', params);
    return this.userService.getAll(params);
  }

  @Get(':id')
  @ApiResponse({ status: 200, description: 'get user by id successful' })
  @ApiResponse({ status: 400, description: 'get user bu id error' })
  @ApiParam({ name: 'id' })
  getDetail(@Param('id', ParseIntPipe) id: number): Promise<User> {
    console.log('get detail user api =>', id);
    return this.userService.getDetail(id);
  }

  @Put(':id')
  @ApiResponse({ status: 200, description: 'update user successful' })
  @ApiResponse({ status: 400, description: 'update user error' })
  @ApiParam({ name: 'id' })
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() data: UpdateUserDto,
  ): Promise<User> {
    console.log('update user api =>', id, data);
    return this.userService.update(id, data);
  }

  @Delete(':id')
  @Roles('admin')
  @ApiResponse({ status: 200, description: 'delete user by id successful' })
  @ApiResponse({ status: 400, description: 'delete user by id error' })
  @ApiParam({ name: 'id' })
  delete(@Param('id', ParseIntPipe) id: number): Promise<User> {
    return this.userService.delete(id);
  }
  @Post('upload-avatar')
  @UseInterceptors(
    FileInterceptor('avatar', {
      storage: storageConfig('avatar'),
      fileFilter: (req, file, cb) => {
        //get extension file
        const ext = extname(file.originalname);
        console.log(ext);
        const allowExtArr = ['.jpg', '.png', '.jpeg'];
        if (!allowExtArr.includes(ext)) {
          req.fileValidationError = `error extension type. Accepted file ext are: ${allowExtArr.toString()}`;
          cb(null, false);
        } else {
          const fileSize = parseInt(req.headers['content-length']);
          if (fileSize > 1024 * 1024 * 5) {
            req.fileValidationError = `File size is too large. Accepted file size is less than 5MB`;
            cb(null, false);
          } else {
            cb(null, true);
          }
        }
      },
    }),
  )
  uploadAvatar(@Req() req: any, @UploadedFile() file: Express.Multer.File) {
    console.log('run api upload avatar');
    console.log('user data', req.user_data);
    console.log(file);
    if (req.fileValidationError) {
      throw new BadRequestException(req.fileValidationError);
    }
    if (!file) {
      throw new BadRequestException('file is required');
    }
    return this.userService.updateAvatar(
      req.user_data.id,
      file.destination + '/' + file.filename,
    );
  }
}
