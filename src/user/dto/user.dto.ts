import { ApiProperty } from '@nestjs/swagger';
import { User } from '@prisma/client';
import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  Matches,
  MinLength,
} from 'class-validator';
import {} from 'class-validator';

export class CreateUserDto {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  @Matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g)
  phone: string;
  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  email: string;
  @ApiProperty()
  @IsNotEmpty()
  @MinLength(6)
  password: string;
  @ApiProperty()
  status: number;
  @ApiProperty()
  image: string;
  @IsOptional()
  refreshToken: string;
}
export class UpdateUserDto {
  @ApiProperty()
  @IsOptional()
  name: string;
  @ApiProperty()
  @IsOptional()
  @Matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g)
  @ApiProperty()
  phone: string;
  @IsOptional()
  @ApiProperty()
  status: number;
  @ApiProperty()
  @IsOptional()
  image: string;
}
export interface UserFilterType {
  items_per_page?: number;
  page?: number;
  search?: string;
}
export interface UserPaginationResponseType {
  data: User[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
