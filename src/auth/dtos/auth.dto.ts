import { ApiProperty } from '@nestjs/swagger';
import {
  Matches,
  IsEmail,
  IsNotEmpty,
  MinLength,
  IsOptional,
} from 'class-validator';

export class RegisterDto {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  @Matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g)
  phone: string;
  @ApiProperty()
  @IsEmail()
  email: string;
  @ApiProperty()
  @MinLength(6)
  password: string;
  @ApiProperty()
  status: number;
  @ApiProperty()
  @IsOptional()
  image: string;
  @IsOptional()
  refreshToken: string;
}
export class LoginDto {
  @ApiProperty()
  @IsEmail()
  email: string;
  @ApiProperty()
  @IsNotEmpty()
  @MinLength(6)
  password: string;
}
