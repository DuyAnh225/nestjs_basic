import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { RegisterDto } from './dtos/auth.dto';
import { User } from '@prisma/client';
import { hash, compare } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private prismaService: PrismaService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}
  register = async (userData: RegisterDto): Promise<User> => {
    // step 1 : checking email has  already userd
    const user = await this.prismaService.user.findUnique({
      where: {
        email: userData.email,
      },
    });
    if (user) {
      throw new HttpException(
        {
          message: 'this email has been used',
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    //step 2: hash password and store to db
    const hashPassword = await hash(userData.password, 10);
    const res = await this.prismaService.user.create({
      data: { ...userData, password: hashPassword },
    });
    return res;
  };
  login = async (data: { email: string; password: string }): Promise<any> => {
    //step 1: checking user is exist by email
    const user = await this.prismaService.user.findUnique({
      where: {
        email: data.email,
      },
    });
    if (!user) {
      throw new HttpException(
        { message: 'account is not exist' },
        HttpStatus.UNAUTHORIZED,
      );
    }
    //step 2: checking password
    const verify = await compare(data.password, user.password);
    if (!verify) {
      throw new HttpException(
        { message: 'password is not correct' },
        HttpStatus.UNAUTHORIZED,
      );
    }
    //step 3: generate access token and refresh token
    const payload = { id: user.id, email: user.email };
    return this.generateToken(payload);
  };

  async refreshToken(refreshToken: string): Promise<any> {
    try {
      const verify = await this.jwtService.verifyAsync(refreshToken, {
        secret: this.configService.get<string>('REFRESH_TOKEN_KEY'),
      });
      console.log(verify);
      const checkExistToken = await this.prismaService.user.findFirst({
        where: {
          email: verify.email,
        },
      });
      if (checkExistToken) {
        return this.generateToken({
          id: verify.id,
          email: verify.email,
        });
      } else {
        throw new HttpException(
          'Refresh token is not valid',
          HttpStatus.BAD_REQUEST,
        );
      }
    } catch (error) {
      throw new HttpException(
        'Refresh token is not valid',
        HttpStatus.BAD_REQUEST,
      );
    }
  }
  private async generateToken(payload: { id: number; email: string }) {
    const accessToken = await this.jwtService.signAsync(payload, {
      secret: this.configService.get<string>('ACCESS_TOKEN_KEY'),
      expiresIn: this.configService.get<string>('EXPIRESIN_ACCESS_TOKEN'),
    });
    const refreshToken = await this.jwtService.signAsync(payload, {
      secret: this.configService.get<string>('REFRESH_TOKEN_KEY'),
      expiresIn: this.configService.get<string>('EXPIRESIN_REFRESH_TOKEN'),
    });
    await this.prismaService.user.update({
      where: {
        email: payload.email,
      },
      data: {
        refreshToken: refreshToken,
      },
    });
    return { accessToken, refreshToken };
  }
}
