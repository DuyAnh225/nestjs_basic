import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    console.log('vào roleguard');
    const requireRoles = this.reflector.getAllAndOverride<string[]>('roles', [
      context.getHandler(),
      context.getClass(),
    ]);

    if (!requireRoles) {
      return true;
    }
    console.log(requireRoles);

    const { user } = context.switchToHttp().getRequest();
    console.log('user ne: ' + user);

    return requireRoles.some((role) => user.roles.split(',').includes(role));
  }
}
