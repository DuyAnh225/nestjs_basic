import { Controller, Post, Body } from '@nestjs/common';
import { User } from '@prisma/client';
import { LoginDto, RegisterDto } from './dtos/auth.dto';
import { AuthService } from './auth.service';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { Public } from './decorator/public.decorator';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post('register')
  @Public()
  @ApiResponse({ status: 201, description: 'Register successful' })
  @ApiResponse({ status: 401, description: 'Register fail!' })
  register(@Body() body: RegisterDto): Promise<User> {
    return this.authService.register(body);
  }
  @Post('login')
  @Public()
  @ApiResponse({ status: 200, description: 'Login successful' })
  @ApiResponse({ status: 400, description: 'Login fail!' })
  login(@Body() body: LoginDto): Promise<any> {
    return this.authService.login(body);
  }
  @Post('refresh-token')
  @Public()
  refreshToken(@Body() { refreshToken }): Promise<any> {
    console.log('refresh api');
    return this.authService.refreshToken(refreshToken);
  }
}
