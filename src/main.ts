import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as dotenv from 'dotenv';
import { ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
dotenv.config();
const port = process.env.PORT;
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  //set up swagger
  const config = new DocumentBuilder()
    .setTitle('Blog APIs')
    .setDescription('List APIs for Blog by Duy Anh')
    .setVersion('1.0')
    .addTag('Auth')
    .addTag('Users')
    .addTag('Posts')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );

  await app.listen(port, () => {
    console.log('server is running at ' + port);
  });
}
bootstrap();
