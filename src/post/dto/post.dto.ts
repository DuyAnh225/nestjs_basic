import { ApiProperty } from '@nestjs/swagger';
import { Post } from '@prisma/client';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreatePostDto {
  @ApiProperty()
  @IsNotEmpty()
  title: string;
  @ApiProperty()
  @IsNotEmpty()
  summary: string;
  @ApiProperty()
  @IsNotEmpty()
  content: string;
  @ApiProperty()
  status: number;
  @ApiProperty()
  @IsNotEmpty()
  ownerId: number;
  @ApiProperty()
  @IsNotEmpty()
  categoryId: number;
}
export class UpdatePostDto {
  @ApiProperty()
  @IsOptional()
  title: string;
  @ApiProperty()
  @IsOptional()
  summary: string;
  @ApiProperty()
  @IsOptional()
  content: string;
  @ApiProperty()
  @IsOptional()
  status: number;
  @ApiProperty()
  @IsOptional()
  ownerId: number;
  @ApiProperty()
  @IsOptional()
  categoryId: number;
}
export interface PostFilterType {
  items_per_page?: number;
  page?: number;
  search?: string;
}
export interface PostPaginationResponseType {
  data: Post[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
