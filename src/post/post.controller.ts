import {
  Body,
  Controller,
  Post,
  Get,
  Query,
  Param,
  ParseIntPipe,
  Put,
  SetMetadata,
} from '@nestjs/common';
import { Post as PostModel } from '@prisma/client';
import { PostService } from './post.service';
import {
  CreatePostDto,
  PostFilterType,
  PostPaginationResponseType,
  UpdatePostDto,
} from './dto/post.dto';
import {
  ApiBearerAuth,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@ApiTags('Posts')
@ApiBearerAuth()
@Controller('posts')
export class PostController {
  constructor(private postService: PostService) {}
  @Post()
  @SetMetadata('roles', ['admin'])
  @ApiResponse({ status: 201, description: 'create post successful' })
  @ApiResponse({ status: 400, description: 'create post error' })
  create(@Body() data: CreatePostDto): Promise<PostModel> {
    return this.postService.create(data);
  }

  @Get()
  @ApiResponse({ status: 200, description: 'get list post successful' })
  @ApiResponse({ status: 400, description: 'get list post error' })
  @ApiQuery({ name: 'page' })
  @ApiQuery({ name: 'items_per_page' })
  @ApiQuery({ name: 'search' })
  getAll(@Query() params: PostFilterType): Promise<PostPaginationResponseType> {
    console.log('get all post ', params);
    return this.postService.getAll(params);
  }

  @Get(':id')
  @ApiResponse({ status: 200, description: 'get post by id successful' })
  @ApiResponse({ status: 400, description: 'get post by id error' })
  @ApiParam({ name: 'id' })
  getDetail(@Param('id', ParseIntPipe) id: number): Promise<PostModel> {
    return this.postService.getDetail(id);
  }

  @Put(':id')
  @SetMetadata('roles', ['admin'])
  @ApiResponse({ status: 200, description: 'update post by id successful' })
  @ApiResponse({ status: 400, description: 'update post by id error' })
  @ApiParam({ name: 'id' })
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() data: UpdatePostDto,
  ): Promise<PostModel> {
    console.log(data);
    return this.postService.update(id, data);
  }
}
